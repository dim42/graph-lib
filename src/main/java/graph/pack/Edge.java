package graph.pack;

public class Edge<T> {
    private final T head;
    private final T tail;

    public Edge(T head, T tail) {
        this.head = head;
        this.tail = tail;
    }

    public T getHead() {
        return head;
    }

    public T getTail() {
        return tail;
    }
}
