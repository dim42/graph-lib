package graph.pack;

import java.util.List;

public interface Graph<T> {

    boolean addVertex(T v);

    boolean addEdge(T v1, T v2);

    List<Edge<T>> getPath(T v1, T v2);
}
