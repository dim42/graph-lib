package graph.pack;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Collections.singletonList;

public class BaseGraph<T> implements Graph<T> {
    private final boolean directed;
    private final Map<T, Set<T>> adjLists = new LinkedHashMap<>();

    public BaseGraph(boolean directed) {
        this.directed = directed;
    }

    @Override
    public boolean addVertex(T v) {
        Set<T> previous = adjLists.putIfAbsent(v, new LinkedHashSet<>());
        return previous == null;
    }

    @Override
    public boolean addEdge(T v1, T v2) {
        if (v1 == null || v2 == null) {
            return false;
        }
        boolean add1 = adjLists.computeIfAbsent(v1, v -> new LinkedHashSet<>())
                .add(v2);
        boolean add2 = true;
        Set<T> adjList = adjLists.computeIfAbsent(v2, v -> new LinkedHashSet<>());
        if (!directed) {
            add2 = adjList.add(v1);
        }
        return add1 && add2;
    }

    @Override
    public List<Edge<T>> getPath(T start, T end) {
        if (!adjLists.containsKey(start) || !adjLists.containsKey(end)) {
            throw new RuntimeException(format("Vertices do not exist in the graph: %s, %s", start, end));
        }
        if (adjLists.get(start).contains(end)) {
            return singletonList(getEdge(start, end));
        }
        Map<T, T> previous = findShortestPreviousVertices(start, end);
        return constructPath(start, end, previous);
    }

    private Map<T, T> findShortestPreviousVertices(T start, T end) {
        List<T> reachable = new LinkedList<>();
        Map<T, T> previous = new HashMap<>();
        for (T neighbor : adjLists.get(start)) {
            reachable.add(neighbor);
            previous.put(neighbor, start);
        }
        List<T> next = new LinkedList<>(adjLists.get(start));
        for (int i = 1; i < adjLists.keySet().size(); i++) {
            List<T> tmpNext = new LinkedList<>();
            for (T current : next) {
                reachable.add(current);
                Set<T> currentNeighbors = adjLists.get(current);
                if (currentNeighbors.contains(end)) {
                    reachable.add(end);
                    previous.put(end, current);
                    return previous;
                }
                for (T neighbor : currentNeighbors) {
                    if (!reachable.contains(neighbor)) {
                        reachable.add(neighbor);
                        tmpNext.add(neighbor);
                        previous.put(neighbor, current);
                    }
                }
            }
            next = tmpNext;
        }
        return previous;
    }

    private List<Edge<T>> constructPath(T start, T end, Map<T, T> previous) {
        T keyVertex = end;
        List<Edge<T>> path = new LinkedList<>();
        while (true) {
            T vertex = previous.get(keyVertex);
            path.add(0, getEdge(vertex, keyVertex));
            if (start.equals(vertex)) {
                break;
            }
            keyVertex = vertex;
        }
        return path;
    }

    private Edge<T> getEdge(T v1, T v2) {
        boolean contains = adjLists.get(v1).contains(v2);
        if (contains) {
            return new Edge<>(v1, v2);
        }
        return null;
    }
}
