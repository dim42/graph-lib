package graph.pack;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GraphTest {
    @Test
    public void testUndirectedGraph() {
        Graph<Integer> graph = new BaseGraph<>(false);

        graph.addVertex(1);
        graph.addEdge(1, 2);
        graph.addEdge(3, 4);
        graph.addEdge(2, 3);
        graph.addEdge(2, 4);
        graph.addVertex(5);
        graph.addEdge(1, 5);
        graph.addEdge(2, 5);
        graph.addEdge(4, 5);
        graph.addEdge(4, 6);

        graph.addVertex(5);
        graph.addEdge(4, 5);
        graph.addEdge(4, null);

        List<Edge<Integer>> path = graph.getPath(1, 6);

        assertThat(path.size(), equalTo(3));
        Edge<Integer> edge = path.get(1);
        assertThat(edge.getHead(), equalTo(2));
        assertThat(edge.getTail(), equalTo(4));
    }

    @Test
    public void testDirectedGraph() {
        Graph<Integer> graph = new BaseGraph<>(true);
        graph.addVertex(1);
        graph.addEdge(1, 2);
        graph.addEdge(4, 2);
        graph.addEdge(1, 4);
        graph.addEdge(2, 5);
        graph.addEdge(5, 4);
        graph.addEdge(3, 5);
        graph.addEdge(3, 6);
        graph.addEdge(6, 6);
        graph.addEdge(4, 7);

        List<Edge<Integer>> path = graph.getPath(1, 7);

        assertThat(path.size(), equalTo(2));
        Edge<Integer> edge = path.get(1);
        assertThat(edge.getHead(), equalTo(4));
        assertThat(edge.getTail(), equalTo(7));
    }
}
